import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Widget> buckets = [];

  String list = "";
  String bucket = "";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: ListView(
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(30),
              height: 60,
              child: Text(
                "BucketList",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic),
              ),
            ),
            Expanded(
                child: TextField(
              style: TextStyle(fontSize: 17),
              textAlign: TextAlign.center,
              onChanged: (txt) {
                setState(() {
                  list = (txt);
                });
              },
              keyboardType: TextInputType.text,
            )),
            RaisedButton(
                onPressed: () {
                  setState(() {
                    bucket = list;
                    buckets.add(Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(30, 5, 30, 5),
                      height: 50,
                      color: Colors.amber[700],
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "$bucket",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          )
                        ],
                      ),
                    ));
                  });
                },
                child: Text("Save")),

            //MyBucketlist
            Container(
                margin: EdgeInsets.all(20),
                alignment: Alignment.center,
                child: Text("My Bucketlist")),

            //Bucketlist
            Column(
              children: buckets,
            )
          ],
        ),
      ),
    );
  }
}
